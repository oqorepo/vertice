<?php

if (isset($_REQUEST['action'])) {
	
	if ($_REQUEST['action'] == "contact_form_request") {

		$ourMail = "francisco.hormazabal@oqo.cl ,  ventas@metra.cl";

		$required_fields = array("name", "email" , "rut" , "tel", "message");
		$pre_messagebody_info = "";
		
		$errors = array();
		$data = array();
			
		parse_str($_REQUEST['values'], $data);
		
		//check for required and assemble message

		if (!empty($data)) {
			foreach ($data as $key => $value) {
				$name = strtolower(trim($key));
				if (in_array($name, $required_fields)) {
					if (empty($value)) {
						
						if ($name == "name") {
							$errors[$name] = "Por favor, ingrese su nombre antes de enviar el mensaje";
						}
						
						if ($name == "rut") {
							$errors[$name] = "Por favor, ingrese su RUT ";
						}
						
						if ($name == "tel") {
							$errors[$name] = "Por favor, ingrese su teléfono de contacto";
						}

						if ($name == "message") {
							$errors[$name] = "Por favor, ingrese su mensaje ";
						}

						if ($name == "email") {
							if (!isValidEmail($value)) {
								$errors[$name] = "Por favor, ingrese correcta dirección de correo electrónico";
							}			
						}
						
					}
				}
			}
		}
		        
		session_start();
        $verify = $_SESSION['verify'];

		if (!empty($errors)) {
			$result['is_errors'] = 1;
			$result['info'] = $errors;
			echo json_encode($result);
			exit;
		}

		$pre_messagebody_info .= "<strong>Nombre</strong>" . ": " . $data['name'] . "<br />";
		$pre_messagebody_info .= "<strong>E-mail</strong>" . ": " . $data['email'] . "<br />";
		$pre_messagebody_info .= "<strong>Rut</strong>" . ": " . $data['rut'] . "<br />";
		$pre_messagebody_info .= "<strong>Teléfono</strong>" . ": " . $data['tel'] . "<br />";

		$headers = 'MIME-Version: 1.0' . "\r\n";
		$headers.= 'Content-type: text/html; charset=UTF-8' . "\r\n";
		$headers.= "From: " . $data['email'] . "\r\n";

		$after_message = "\r\n<br />--------------------------------------------------------------------------------------------------\r\n<br /> Este correo electrónico fue enviado a través del formulario de contacto de www.edificiovertice.cl";

		if (mail($ourMail, "Mensaje enviado del formulario de contacto de  Vértice", $pre_messagebody_info .="<strong>Mensaje</strong>" . ": " . nl2br($data['message']) . $after_message, $headers)) {
			$result["info"] = "success";
		} else {
			$result["info"] = "server_fail";
		}

		echo json_encode($result);
		exit;
	}
}

function isValidEmail($email) {
	return filter_var($email, FILTER_VALIDATE_EMAIL);
}

?>

